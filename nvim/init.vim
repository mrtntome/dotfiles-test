" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.config/nvim/plugged')

" Declare the list of plugins.
Plug 'LnL7/vim-nix'
Plug 'lambdalisue/suda.vim'
" List ends here. Plugins become visible to Vim after this call.
call plug#end()

let g:suda_smart_edit = 1
